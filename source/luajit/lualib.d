module luajit.lualib;
/*
** Standard library header.
** Copyright (C) 2005-2017 Mike Pall. See Copyright Notice in luajit.h
*/

import luajit.lua;

extern (C):

enum LUA_FILEHANDLE = "FILE*";

enum LUA_COLIBNAME = "coroutine";
enum LUA_MATHLIBNAME = "math";
enum LUA_STRLIBNAME = "string";
enum LUA_TABLIBNAME = "table";
enum LUA_IOLIBNAME = "io";
enum LUA_OSLIBNAME = "os";
enum LUA_LOADLIBNAME = "package";
enum LUA_DBLIBNAME = "debug";
enum LUA_BITLIBNAME = "bit";
enum LUA_JITLIBNAME = "jit";
enum LUA_FFILIBNAME = "ffi";

int luaopen_base (lua_State* L);
int luaopen_math (lua_State* L);
int luaopen_string (lua_State* L);
int luaopen_table (lua_State* L);
int luaopen_io (lua_State* L);
int luaopen_os (lua_State* L);
int luaopen_package (lua_State* L);
int luaopen_debug (lua_State* L);
int luaopen_bit (lua_State* L);
int luaopen_jit (lua_State* L);
int luaopen_ffi (lua_State* L);

void luaL_openlibs (lua_State* L);

extern (D) auto lua_assert(T)(auto ref T x)
{
    return cast(void) 0;
}
