module luajit.luajit;
/*
** LuaJIT -- a Just-In-Time Compiler for Lua. http://luajit.org/
**
** Copyright (C) 2005-2017 Mike Pall. All rights reserved.
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
*/

import luajit.lua;

extern (C):

enum LUAJIT_VERSION = "LuaJIT 2.0.5";
enum LUAJIT_VERSION_NUM = 20005; /* Version 2.0.5 = 02.00.05. */
alias LUAJIT_VERSION_SYM = luaJIT_version_2_0_5;
enum LUAJIT_COPYRIGHT = "Copyright (C) 2005-2017 Mike Pall";
enum LUAJIT_URL = "http://luajit.org/";

/* Modes for luaJIT_setmode. */
enum LUAJIT_MODE_MASK = 0x00ff;

enum
{
    LUAJIT_MODE_ENGINE = 0, /* Set mode for whole JIT engine. */
    LUAJIT_MODE_DEBUG = 1, /* Set debug mode (idx = level). */

    LUAJIT_MODE_FUNC = 2, /* Change mode for a function. */
    LUAJIT_MODE_ALLFUNC = 3, /* Recurse into subroutine protos. */
    LUAJIT_MODE_ALLSUBFUNC = 4, /* Change only the subroutines. */

    LUAJIT_MODE_TRACE = 5, /* Flush a compiled trace. */

    LUAJIT_MODE_WRAPCFUNC = 0x10, /* Set wrapper mode for C function calls. */

    LUAJIT_MODE_MAX = 17
}

/* Flags or'ed in to the mode. */
enum LUAJIT_MODE_OFF = 0x0000; /* Turn feature off. */
enum LUAJIT_MODE_ON = 0x0100; /* Turn feature on. */
enum LUAJIT_MODE_FLUSH = 0x0200; /* Flush JIT-compiled code. */

/* LuaJIT public C API. */

/* Control the JIT engine. */
int luaJIT_setmode (lua_State* L, int idx, int mode);

/* Enforce (dynamic) linker error for version mismatches. Call from main. */
void luaJIT_version_2_0_5 ();

