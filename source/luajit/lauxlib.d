module luajit.lauxlib;
/*
** $Id: lauxlib.h,v 1.88.1.1 2007/12/27 13:02:25 roberto Exp $
** Auxiliary functions for building Lua libraries
** See Copyright Notice in lua.h
*/

import core.stdc.config;

import core.stdc.stdio;

import luajit.lua;
import luajit.luaconf;

extern (C):

extern (D) int luaL_getn(lua_State* L, int i)
{
    return cast(int) lua_objlen(L, i);
}

extern (D) auto luaL_setn(T2)(lua_State* L, int i, auto ref T2 j)
{
    return cast(void) 0;
} /* no op! */

/* extra error code for `luaL_load' */
enum LUA_ERRFILE = LUA_ERRERR + 1;

struct luaL_Reg
{
    const(char)* name;
    lua_CFunction func;
}

void luaL_openlib (
    lua_State* L,
    const(char)* libname,
    const(luaL_Reg)* l,
    int nup);
void luaL_register (lua_State* L, const(char)* libname, const(luaL_Reg)* l);
int luaL_getmetafield (lua_State* L, int obj, const(char)* e);
int luaL_callmeta (lua_State* L, int obj, const(char)* e);
int luaL_typerror (lua_State* L, int narg, const(char)* tname);
int luaL_argerror (lua_State* L, int numarg, const(char)* extramsg);
const(char)* luaL_checklstring (lua_State* L, int numArg, size_t* l);
const(char)* luaL_optlstring (
    lua_State* L,
    int numArg,
    const(char)* def,
    size_t* l);
lua_Number luaL_checknumber (lua_State* L, int numArg);
lua_Number luaL_optnumber (lua_State* L, int nArg, lua_Number def);

lua_Integer luaL_checkinteger (lua_State* L, int numArg);
lua_Integer luaL_optinteger (lua_State* L, int nArg, lua_Integer def);

void luaL_checkstack (lua_State* L, int sz, const(char)* msg);
void luaL_checktype (lua_State* L, int narg, int t);
void luaL_checkany (lua_State* L, int narg);

int luaL_newmetatable (lua_State* L, const(char)* tname);
void* luaL_checkudata (lua_State* L, int ud, const(char)* tname);

void luaL_where (lua_State* L, int lvl);
int luaL_error (lua_State* L, const(char)* fmt, ...);

int luaL_checkoption (
    lua_State* L,
    int narg,
    const(char)* def,
    const(char*)* lst);

int luaL_ref (lua_State* L, int t);
void luaL_unref (lua_State* L, int t, int ref_);

int luaL_loadfile (lua_State* L, const(char)* filename);
int luaL_loadbuffer (
    lua_State* L,
    const(char)* buff,
    size_t sz,
    const(char)* name);
int luaL_loadstring (lua_State* L, const(char)* s);

lua_State* luaL_newstate ();

const(char)* luaL_gsub (
    lua_State* L,
    const(char)* s,
    const(char)* p,
    const(char)* r);

const(char)* luaL_findtable (
    lua_State* L,
    int idx,
    const(char)* fname,
    int szhint);

/* From Lua 5.2. */
int luaL_fileresult (lua_State* L, int stat, const(char)* fname);
int luaL_execresult (lua_State* L, int stat);
int luaL_loadfilex (lua_State* L, const(char)* filename, const(char)* mode);
int luaL_loadbufferx (
    lua_State* L,
    const(char)* buff,
    size_t sz,
    const(char)* name,
    const(char)* mode);
void luaL_traceback (lua_State* L, lua_State* L1, const(char)* msg, int level);

/*
** ===============================================================
** some useful macros
** ===============================================================
*/

extern (D) auto luaL_argcheck(T0, T1, T2, T3)(auto ref T0 L, auto ref T1 cond, auto ref T2 numarg, auto ref T3 extramsg)
{
    return cast(void) cond || luaL_argerror(L, numarg, extramsg);
}

extern (D) const (char*) luaL_checkstring (lua_State *L, int n){return luaL_checklstring(L, (n), null);}

extern (D) auto luaL_optstring(T0, T1, T2)(auto ref T0 L, auto ref T1 n, auto ref T2 d)
{
    return luaL_optlstring(L, n, d, NULL);
}

extern (D) auto luaL_checkint(T0, T1)(auto ref T0 L, auto ref T1 n)
{
    return cast(int) luaL_checkinteger(L, n);
}

extern (D) auto luaL_optint(T0, T1, T2)(auto ref T0 L, auto ref T1 n, auto ref T2 d)
{
    return cast(int) luaL_optinteger(L, n, d);
}

extern (D) auto luaL_checklong(T0, T1)(auto ref T0 L, auto ref T1 n)
{
    return cast(c_long) luaL_checkinteger(L, n);
}

extern (D) auto luaL_optlong(T0, T1, T2)(auto ref T0 L, auto ref T1 n, auto ref T2 d)
{
    return cast(c_long) luaL_optinteger(L, n, d);
}

extern (D) auto luaL_typename(T0, T1)(auto ref T0 L, auto ref T1 i)
{
    return lua_typename(L, lua_type(L, i));
}

extern (D) int luaL_dofile(lua_State* L, const(char)* fn){return luaL_loadfile(L, fn) || lua_pcall(L, 0, LUA_MULTRET, 0);}

extern (D) auto luaL_dostring(T0, T1)(auto ref T0 L, auto ref T1 s)
{
    return luaL_loadstring(L, s) || lua_pcall(L, 0, LUA_MULTRET, 0);
}

extern (D) auto luaL_getmetatable(T0, T1)(auto ref T0 L, auto ref T1 n)
{
    return lua_getfield(L, LUA_REGISTRYINDEX, n);
}

extern (D) auto luaL_opt(T0, T1, T2, T3)(auto ref T0 L, auto ref T1 f, auto ref T2 n, auto ref T3 d)
{
    return lua_isnoneornil(L, n) ? d : f(L, n);
}

/*
** {======================================================
** Generic Buffer manipulation
** =======================================================
*/

struct luaL_Buffer
{
    char* p; /* current position in buffer */
    int lvl; /* number of strings in the stack (level) */
    lua_State* L;
    char[LUAL_BUFFERSIZE] buffer;
}

extern (D) auto luaL_addchar(luaL_Buffer* B, char c)
{
	cast(void)(B.p < (B.buffer.ptr+LUAL_BUFFERSIZE) || luaL_prepbuffer(B));
	*B.p++ = cast(char)(c);
}

/* compatibility only */
alias luaL_putchar = luaL_addchar;

void luaL_buffinit (lua_State* L, luaL_Buffer* B);
char* luaL_prepbuffer (luaL_Buffer* B);
void luaL_addlstring (luaL_Buffer* B, const(char)* s, size_t l);
void luaL_addstring (luaL_Buffer* B, const(char)* s);
void luaL_addvalue (luaL_Buffer* B);
void luaL_pushresult (luaL_Buffer* B);

/* }====================================================== */

/* compatibility with ref system */

/* pre-defined references */
enum LUA_NOREF = -2;
enum LUA_REFNIL = -1;

extern (D) auto lua_unref(T0, T1)(auto ref T0 L, auto ref T1 ref_)
{
    return luaL_unref(L, LUA_REGISTRYINDEX, ref_);
}

extern (D) auto lua_getref(T0, T1)(auto ref T0 L, auto ref T1 ref_)
{
    return lua_rawgeti(L, LUA_REGISTRYINDEX, ref_);
}

alias luaL_reg = luaL_Reg;

