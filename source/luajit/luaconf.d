module luajit.luaconf;
/*
** Configuration header.
** Copyright (C) 2005-2017 Mike Pall. See Copyright Notice in luajit.h
*/

import core.stdc.config;

import core.stdc.stddef;
import core.stdc.stdio;

import luajit.lua;

extern (C):

enum WINVER = 0x0501;

/* Default path for loading Lua and C modules with require(). */

/*
** In Windows, any exclamation mark ('!') in the path is replaced by the
** path of the directory of the executable file of the current process.
*/

/*
** Note to distribution maintainers: do NOT patch the following lines!
** Please read ../doc/install.html#distro and pass PREFIX=/usr instead.
*/

enum LUA_MULTILIB = "lib";

enum LUA_LMULTILIB = "lib";

enum LUA_LROOT = "/usr/local";
enum LUA_LUADIR = "/lua/5.1/";
enum LUA_LJDIR = "/luajit-2.0.5/";

enum LUA_JROOT = LUA_LROOT;

/* Environment variable names for path overrides and initialization code. */
enum LUA_PATH = "LUA_PATH";
enum LUA_CPATH = "LUA_CPATH";
enum LUA_INIT = "LUA_INIT";

/* Special file system characters. */

enum LUA_DIRSEP = "/";

enum LUA_PATHSEP = ";";
enum LUA_PATH_MARK = "?";
enum LUA_EXECDIR = "!";
enum LUA_IGMARK = "-";

/* Quoting in error messages. */
//alias LUA_QL(x)	"'" x "'"
//enum LUA_QS = LUA_QL("%s");

/* Various tunables. */
enum LUAI_MAXSTACK = 65500; /* Max. # of stack slots for a thread (<64K). */
enum LUAI_MAXCSTACK = 8000; /* Max. # of stack slots for a C func (<10K). */
enum LUAI_GCPAUSE = 200; /* Pause GC until memory is at 200%. */
enum LUAI_GCMUL = 200; /* Run GC at 200% of allocation speed. */
enum LUA_MAXCAPTURES = 32; /* Max. pattern captures. */

/* Compatibility with older library function names. */ /* OLD: math.mod, NEW: math.fmod */ /* OLD: string.gfind, NEW: string.gmatch */

/* Configuration for the frontend (the luajit executable). */

/* Fallback frontend name. */
/* Interactive prompt. */
/* Continuation prompt. */
/* Max. input line length. */

/* Note: changing the following defines breaks the Lua 5.1 ABI. */
alias LUA_INTEGER = ptrdiff_t;
enum LUA_IDSIZE = 60; /* Size of lua_Debug.short_src. */
/*
** Size of lauxlib and io.* on-stack buffers. Weird workaround to avoid using
** unreasonable amounts of stack space, but still retain ABI compatibility.
** Blame Lua for depending on BUFSIZ in the ABI, blame **** for wrecking it.
*/
enum LUAL_BUFFERSIZE = BUFSIZ > 16384 ? 8192 : BUFSIZ;

/* The following defines are here only for compatibility with luaconf.h
** from the standard Lua distribution. They must not be changed for LuaJIT.
*/
alias LUA_NUMBER = double;
alias LUAI_UACNUMBER = double;
enum LUA_NUMBER_SCAN = "%lf";
enum LUA_NUMBER_FMT = "%.14g";

extern (D) int lua_number2str(T1)(const char* s, auto ref T1 n)
{
    return sprintf(s, LUA_NUMBER_FMT, n);
}

enum LUAI_MAXNUMBER2STR = 32;
enum LUA_INTFRMLEN = "l";
alias LUA_INTFRM_T = c_long;

/* Linkage of public API functions. */

//enum LUALIB_API = LUA_API;

/* Support for internal assertions. */

